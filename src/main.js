import Vue from 'vue'
import App from './App.vue'
import VueRecord from '@codekraft-studio/vue-record'
import axios from 'axios'
import {API_HOST } from './constants'
import VModal from 'vue-js-modal'
import VueCookies from 'vue-cookies';

import router from './router/index.js'

Vue.use(VueCookies);

Vue.config.productionTip = false

axios.interceptors.request.use(config => {
  config.url = API_HOST + config.url
  config.withCredentials = true
  return config
}, error => {
  return Promise.reject(error)
})


Vue.prototype.$http = axios

Vue.use(VueRecord)
Vue.use(VModal)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
