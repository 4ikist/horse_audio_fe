import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '@/components/LoginPage.vue'
import InputForm from '@/components/InputForm.vue'

Vue.use(VueRouter)

export default new VueRouter({
  routes: [
    {
      path: '/login',
      name: 'login-page',
      component: LoginPage
    },
    {
      path: '/input-form',
      name: 'input-form',
      component: InputForm
    }
  ]
})
